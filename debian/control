Source: versiontools
Maintainer: Benjamin Drung <benjamin.drung@cloud.ionos.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-distutils,
               python3-setuptools
Standards-Version: 4.4.0
Homepage: https://launchpad.net/versiontools
Vcs-Browser: https://salsa.debian.org/debian/versiontools
Vcs-Git: https://salsa.debian.org/debian/versiontools.git

Package: python3-versiontools
Architecture: all
Depends: python3-distutils, ${misc:Depends}, ${python3:Depends}
Description: Smart replacement for plain tuple used in __version__ (Python 3)
 versiontools is a Python module providing a smart replacement for plain tuple
 used in __version__. It has following features:
 .
  * A piece of code that allows you to keep a single version definition inside
    your package or module. No more hacks in setup.py, no more duplicates in
    setup.py and somewhere else. Just one version per package.
  * Version objects can produce nice version strings for released files that
    are compliant with PEP 386. Releases, alphas, betas, development snaphots.
    All of those get good version strings out of the box.
  * Version objects understand the VCS used by your project. Git, Mercurial and
    Bazaar are supported out of the box. Custom systems can be added by 3rd
    party plugins.
  * Version object that compares as a tuple of values and sorts properly.
  * Zero-dependency install! If all you care about is handling setup() to get
    nice tarball names then you don’t need to depend on versiontools (no
    setup_requires, no install_requires!). You will need to bundle a small
    support module though.
 .
 This is the Python 3 version of the package.
